defmodule Example.Size do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          inches: integer
        }

  defstruct inches: 0

  field :inches, 1, type: :int32
end
defmodule Example.Hat do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          inches: integer,
          color: String.t(),
          name: String.t()
        }

  defstruct inches: 0,
            color: "",
            name: ""

  field :inches, 1, type: :int32
  field :color, 2, type: :string
  field :name, 3, type: :string
end
